﻿using System;
using System.Collections.Generic;
using System.Text;
using Models.Models;
using System.Linq;
using Models.LINQQueryResults;

namespace _01_linq
{
    public static class PrintSolution
    {
        public static void task_01(List<Project> data, int authorId)
        {
            Dictionary<Project, int> result = Solution.task_01(data, authorId);

            Console.WriteLine(new string('-', 20));
            Console.WriteLine("Task01");
            Console.WriteLine(new string('-', 20));
            foreach (var pair in result)
            {
                Console.WriteLine("\t" + new string('*', 20));
                Console.WriteLine("\t" + "ProjectId = " + pair.Key.Id);
                Console.WriteLine("\t" + "Task count = " + pair.Value);
                Console.WriteLine("\t" + new string('*', 20));
            }
        }

        public static void task_02(List<Project> data, int userId)
        {
            IEnumerable<Task> result = Solution.task_02(data, userId);


            Console.WriteLine(new string('-', 20));
            Console.WriteLine("Task02");
            Console.WriteLine(new string('-', 20));
            foreach (var t in result)
            {
                Console.WriteLine("\t" + new string('*', 20));
                Console.WriteLine("\t" + "TaskId = " + t.Id);
                Console.WriteLine("\t" + "Name = " + t.Name);
                Console.WriteLine("\t" + new string('*', 20));
            }
        }

        public static void task_03(List<Project> data, int userId)
        {
            IEnumerable<Task03Result> result = Solution.task_03(data, userId);

            Console.WriteLine(new string('-', 20));
            Console.WriteLine("Task03");
            Console.WriteLine(new string('-', 20));
            foreach (var t in result)
            {
                Console.WriteLine("\t" + new string('*', 20));
                Console.WriteLine("\t" + "TaskId = " + t.Id);
                Console.WriteLine("\t" + "Name = " + t.Name);
                Console.WriteLine("\t" + new string('*', 20));
            }
        }

        public static void task_04(List<Project> data)
        {
            IEnumerable<Task04Result> result = Solution.task_04(data);

            Console.WriteLine(new string('-', 20));
            Console.WriteLine("Task04");
            Console.WriteLine(new string('-', 20));
            foreach (var t in result)
            {
                Console.WriteLine(new string('*', 20));
                Console.WriteLine("TeamId = " + t.Id);
                Console.WriteLine("Name = " + t.Name);
                foreach (var u in t.Users)
                {
                    Console.WriteLine("\tUserId = " + u.Id);
                    Console.WriteLine("\tName = " + u.FirstName);
                    Console.WriteLine("\tBirthDay = " + u.BirthDay);
                    Console.WriteLine("\tRegisterAt = " + u.RegisteredAt);
                    Console.WriteLine("\t-----");

                }
                Console.WriteLine(new string('*', 20));
            }
        }

        public static void task_05(List<Project> data)
        {
            IEnumerable<Task05Result> result = Solution.task_05(data);

            Console.WriteLine(new string('-', 20));
            Console.WriteLine("Task05");
            Console.WriteLine(new string('-', 20));
            foreach (var u in result)
            {
                Console.WriteLine(new string('*', 20));
                Console.WriteLine("UserId = " + u.User.Id);
                Console.WriteLine("Name = " + u.User.FirstName);
                foreach (var t in u.Tasks)
                {
                    Console.WriteLine("\tTaskId = " + t.Id);
                    Console.WriteLine("\tName = " + t.Name);
                    Console.WriteLine("\t-----");

                }
                Console.WriteLine(new string('*', 20));
            }
        }

        public static void task_06(List<Project> data, int userId)
        {
            IEnumerable<Task06Result> result = Solution.task_06(data, userId);

            Console.WriteLine(new string('-', 20));
            Console.WriteLine("Task06");
            Console.WriteLine(new string('-', 20));
            foreach (var info in result)
            {
                Console.WriteLine("\t" + new string('*', 20));
                Console.WriteLine("\tUserId = " + info.User.Id);

                if (info.LastProject != null)
                {
                    Console.WriteLine("\tlastProjectId = " + info.LastProject.Id);
                    Console.WriteLine("\tlastProjectTasksAmount = " + info.LastProjectTasksCount);
                }
                Console.WriteLine("\tunfinishedTasksAmount = " + info.UnfinishedTasksCount);

                if (info.LongestTask != null)
                {
                    Console.WriteLine("\tlongestTask.createdAt = " + info.LongestTask.CreatedAt);
                    Console.WriteLine("\tlongestTask.finishedAt = " + info.LongestTask.FinishedAt);
                }
                Console.WriteLine("\t" + new string('*', 20));
            }
        }

        public static void task_07(List<Project> data)
        {
            IEnumerable<Task07Result> result = Solution.task_07(data);

            Console.WriteLine(new string('-', 20));
            Console.WriteLine("Task07");
            Console.WriteLine(new string('-', 20));
            foreach (var info in result)
            {
                Console.WriteLine(new string('*', 20));
                Console.WriteLine("projectId = " + info.Project.Id);
                Console.WriteLine("longestTask = " + (info.LongestTask != null ? info.LongestTask.Id.ToString() : "null"));
                Console.WriteLine("shortestTask = " + (info.ShortestTask != null ? info.ShortestTask.Id.ToString() : "null"));
                Console.WriteLine("usersInTeamCount = " + (info.UsersInTeamCount != null ? info.UsersInTeamCount.ToString() : "null"));
                Console.WriteLine(new string('*', 20));
            }
        }
    }
}
