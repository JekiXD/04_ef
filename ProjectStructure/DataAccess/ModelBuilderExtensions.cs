﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Models.Models;

namespace DataAccess
{
    public static class ModelBuilderExtensions
    {
        public static void Configure(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Task>()
               .HasOne(t => t.Performer)
               .WithMany()
               .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<User>()
                .HasOne(u => u.Team)
                .WithMany()
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Project>()
                .HasOne(p => p.Author)
                .WithMany()
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Project>()
                .HasOne(p => p.Team)
                .WithMany()
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Project>()
                .HasMany(p => p.Tasks)
                .WithOne(t => t.Project);
        }

        public static void Seed(this ModelBuilder modelBuilder)
        {
            var teams = new List<Team>()
            {
                new Team{Id = 1, Name = "FisrtTeam" },
                new Team{Id = 2, Name = "SecondTeam" },
            };

            var users = new List<User>()
            {
                new User{Id = 1, TeamId = 1, FirstName = "Jake", BirthDay = DateTime.Now},
                new User{Id = 2, TeamId = 2, FirstName = "Anna", BirthDay = DateTime.Now},
                new User{Id = 3,             FirstName = "Daniel", BirthDay = DateTime.Now},
                new User{Id = 4, TeamId = 1, FirstName = "Sasuke", BirthDay = DateTime.Now},
                new User{Id = 5, TeamId = 2, FirstName = "Megumi", BirthDay = DateTime.Now},
                new User{Id = 6, TeamId = 2, FirstName = "Caron", BirthDay = DateTime.Now},
            };

            var projects = new List<Project>()
            {
                new Project{Id = 1, AuthorId = 2, TeamId = 2, Name = "FirstProject", Deadline = DateTime.Now.AddDays(160)},
                new Project{Id = 2, AuthorId = 4, TeamId = 1, Name = "SecondProject", Deadline = DateTime.Now.AddDays(90)},
            };

            var tasks = new List<Task>()
            {
                new Task{Id = 1, ProjectId = 1, PerformerId = 6, Name = "FirstTask", State = 2},
                new Task{Id = 2, ProjectId = 1, PerformerId = 6, Name = "SecondTask", State = 2},
                new Task{Id = 3, ProjectId = 1, PerformerId = 2, Name = "ThirdTask", State = 2},
                new Task{Id = 4, ProjectId = 1, PerformerId = 2, Name = "FourthTask", State = 2},
                new Task{Id = 5, ProjectId = 1, PerformerId = 5, Name = "FifthTask", State = 2},
                new Task{Id = 6, ProjectId = 2, PerformerId = 1, Name = "SixthTask", State = 2},
                new Task{Id = 7, ProjectId = 2, PerformerId = 4, Name = "SeventhTask", State = 2},
                new Task{Id = 8, ProjectId = 2, PerformerId = 4, Name = "EighthTask", State = 2},
                new Task{Id = 9, ProjectId = 2, PerformerId = 1, Name = "NinthTask", State = 2},
            };

            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Task>().HasData(tasks);
        }
    }
}
