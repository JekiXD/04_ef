﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Server.Services;
using Models.DTO;
using Server.Interfaces;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        readonly ITaskService _service;
        public TaskController(ITaskService service)
        {
            _service = service;
        }

        [HttpGet]
        [Route("read")]
        public ActionResult<IEnumerable<TaskDTO>> Read()
        {
            return Ok(_service.GetTasks());
        }

        [HttpGet]
        [Route("read/{id}")]
        public ActionResult<TaskDTO> Read(int id)
        {
            TaskDTO task = _service.GetTaskById(id);

            if (task == null) return NotFound();
            return Ok(task);
        }

        [HttpPost]
        [Route("create")]
        public IActionResult Create([FromBody] TaskDTO task)
        {
            _service.AddTask(task);
            return NoContent();
        }

        [HttpPut]
        [Route("update")]
        public IActionResult Update([FromBody] TaskDTO task)
        {
            _service.UpdateTask(task);
            return NoContent();
        }

        [HttpDelete]
        [Route("delete/{id}")]
        public IActionResult Delete(int id)
        {
            if (!_service.ExistsTask(id)) return NotFound();
            _service.DeleteTask(id);
            return NoContent();
        }
    }
}
