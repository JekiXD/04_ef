﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Server.Services;
using Models.DTO;
using System.Net;
using Server.Interfaces;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        readonly IUserService _service;
        public UserController(IUserService service)
        {
            _service = service;
        }

        [HttpGet]
        [Route("read")]
        public ActionResult<IEnumerable<UserDTO>> Read()
        {
            return Ok(_service.GetUsers());
        }

        [HttpGet]
        [Route("read/{id}")]
        public ActionResult<UserDTO> Read(int Id)
        {
            UserDTO user = _service.GetUserById(Id);

            if (user == null) return NotFound();
            return Ok(user);
        }

        [HttpPost]
        [Route("create")]
        public IActionResult Create([FromBody] UserDTO user)
        {
            if (_service.ExistsUser(user.Id)) return Conflict("Such user already exists");
            var newUser = _service.AddUser(user);
            return StatusCode((int)HttpStatusCode.Created, newUser);
        }

        [HttpPut]
        [Route("update")]
        public IActionResult Update([FromBody] UserDTO user)
        {
            _service.UpdateUser(user);
            return NoContent();
        }

        [HttpDelete]
        [Route("delete/{id}")]
        public IActionResult Delete(int id)
        {
            if (!_service.ExistsUser(id)) return NotFound();
            _service.DeleteUser(id);
            return NoContent();
        }
    }
}
