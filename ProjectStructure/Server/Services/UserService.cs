﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Server.Repositories;
using Server.Interfaces;
using Models.DTO;
using AutoMapper;
using MM = Models.Models;

namespace Server.Services
{
    public class UserService : IUserService
    {
        readonly IUnitOfWork _unitOfWork;
        readonly IRepository<MM.User> _repository;
        readonly IRepository<MM.Team> _teamRepository;
        readonly IRepository<MM.Task> _taskRepository;
        readonly IMapper _mapper;
        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _repository = _unitOfWork.Set<MM.User>();
            _teamRepository = _unitOfWork.Set<MM.Team>();
            _taskRepository = _unitOfWork.Set<MM.Task>();
            _mapper = mapper;
        }

        public IEnumerable<UserDTO> GetUsers()
        {
            var userSet = _repository.Get();

            return _mapper.Map<IEnumerable<UserDTO>>(userSet);
        }

        public UserDTO GetUserById(int id)
        {
            var userSet = _repository.Get(u => u.Id == id);

            if (!userSet.Any()) return null;

            return _mapper.Map<UserDTO>(userSet.First());
        }

        public UserDTO AddUser(UserDTO user)
        {
            MM.User newUser = _mapper.Map<MM.User>(user);
            var team = getUserTeam(user);

            newUser.Team = team;
            _repository.Create(newUser);
            _unitOfWork.SaveChanges();

            return _mapper.Map<UserDTO>(newUser);
        }

        public void UpdateUser(UserDTO user)
        {
            MM.User newUser = _mapper.Map<MM.User>(user);
            var team = getUserTeam(user);

            newUser.Team = team;
            _repository.Update(newUser);
            _unitOfWork.SaveChanges();
        }

        public void DeleteUser(int id)
        {
            _repository.Delete(id);
            _unitOfWork.SaveChanges();
        }

        public MM.Team getUserTeam(UserDTO user)
        {
            var team = _teamRepository.Get(t => t.Id == user.TeamId);

            return team.FirstOrDefault();
        }

        public bool ExistsUser(int id)
        {
            var users = _repository.Get(u => u.Id == id);
            return users.Any();
        }
    }
}
