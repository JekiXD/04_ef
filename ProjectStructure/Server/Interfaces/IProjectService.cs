﻿using Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Interfaces
{
    public interface IProjectService
    {
        IEnumerable<ProjectDTO> GetProjects();
        ProjectDTO GetProjectById(int id);
        ProjectDTO AddProject(ProjectDTO project);
        void UpdateProject(ProjectDTO project);
        void DeleteProject(int id);
        public bool ExistsProject(int id);
    }
}
