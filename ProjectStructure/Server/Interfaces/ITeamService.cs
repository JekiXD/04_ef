﻿using Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Interfaces
{
    public interface ITeamService
    {
        IEnumerable<TeamDTO> GetTeams();
        TeamDTO GetTeamById(int id);
        TeamDTO AddTeam(TeamDTO team);
        void UpdateTeam(TeamDTO team);
        void DeleteTeam(int id);
        bool ExistsTeam(int id);
    }
}
