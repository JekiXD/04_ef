﻿using Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Interfaces
{
    public interface ITaskService
    {
        IEnumerable<TaskDTO> GetTasks();
        TaskDTO GetTaskById(int id);
        void AddTask(TaskDTO task);
        void UpdateTask(TaskDTO task);
        void DeleteTask(int id);
        bool ExistsTask(int id);
    }
}
