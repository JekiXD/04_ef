﻿using Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Interfaces
{
    public interface IUserService
    {
        IEnumerable<UserDTO> GetUsers();
        UserDTO GetUserById(int id);
        UserDTO AddUser(UserDTO user);
        void UpdateUser(UserDTO user);
        void DeleteUser(int id);
        bool ExistsUser(int id);
    }
}
