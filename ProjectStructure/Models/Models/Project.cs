﻿using Models.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Models
{
    public class Project : BaseEntity
    {
        public Project()
        {
            Tasks = new List<Task>();
        }
        public int? AuthorId { get; set; }
        public User Author { get; set; }
        public int? TeamId { get; set; }
        public Team Team { get; set; }
        public ICollection<Task> Tasks { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
    }
}
