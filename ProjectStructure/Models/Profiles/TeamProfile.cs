﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Models.DTO;
using Models.Models;

namespace Models.Profiles
{
    public class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDTO>();

            CreateMap<TeamDTO, Team>();
        }
    }
}
